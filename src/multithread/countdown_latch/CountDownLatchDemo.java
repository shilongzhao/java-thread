package multithread.countdown_latch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * author: zhaoshilong
 * date: 10/08/2017
 */
public class CountDownLatchDemo {
    public static void main(String[] args) {
        CountDownLatch latch = new CountDownLatch(3);
        // let threads wait until latch count reaches 0. latch: 原意是指门后用于锁门的门闩

        ExecutorService executorService = Executors.newFixedThreadPool(3);

        for (int i = 0; i < 3; i++) {
            executorService.submit(new Processor(latch)); // each thread will count down latch by 1.
        }

        executorService.shutdown(); // remember to shutdown
        try {
            latch.await(); // main thread will only carry on when latch count reaches 0.
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Completed!");
    }
}


class Processor implements Runnable {
    private CountDownLatch latch; // no need to synchronize on latch: CountDownLatch is thread safe.
    public Processor(CountDownLatch latch) {
        this.latch = latch;
    }


    @Override
    public void run() {
        System.out.println("Started .. ");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        latch.countDown();
    }
}