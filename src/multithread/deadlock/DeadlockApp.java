package multithread.deadlock;

/**
 * @auther s.zhao@opengate.biz
 */
public class DeadlockApp {
    public static void main(String[] args) throws InterruptedException {
        final DeadlockRunner runner = new DeadlockRunner();

        Thread t1 = new Thread(() -> {
            try {
                runner.firstThread();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            try {
                runner.secondThread();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        runner.finished();
    }
}
