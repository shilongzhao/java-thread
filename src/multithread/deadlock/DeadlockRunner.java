package multithread.deadlock;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @auther s.zhao@opengate.biz
 */
public class DeadlockRunner {
    private Account acc1 = new Account();
    private Account acc2 = new Account();

    private Lock lock1 = new ReentrantLock();
    private Lock lock2 = new ReentrantLock();

    private void aquireLocks(Lock first, Lock second) throws InterruptedException {
        while (true) {
            // Acquire locks
            boolean gotFirstLock = false;
            boolean gotSecondLock = false;
            try {
                gotFirstLock = first.tryLock(); // try to get lock on the first, return true if succeeded
                gotSecondLock = second.tryLock();
            }
            finally {
                if (gotFirstLock & gotSecondLock)
                    return;
                if (gotFirstLock)
                    first.unlock();
                if (gotSecondLock)
                    second.unlock();
            }

            Thread.sleep(1);
        }
    }
    public void firstThread() throws InterruptedException {
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            aquireLocks(lock1, lock2);
            //            lock1.lock(); // acquire lock
//            lock2.lock(); // acquire lock
            try {
                Account.transfer(acc1, acc2, random.nextInt(100));
            } finally {
                lock1.unlock();
                lock2.unlock();
            }
        }
    }

    public void secondThread() throws InterruptedException {
        Random random = new Random();
        for (int i = 0; i < 10001; i++) {
            aquireLocks(lock2, lock1); // not no worry about deadlock
//            lock1.lock();
//            lock2.lock(); // program could be in dead lock if we change the lock1 and lock2 orders here
            try {
                Account.transfer(acc2, acc1, random.nextInt(100));
            } finally {
                lock1.unlock();
                lock2.unlock();
            }
        }
    }

    public void finished() {
        System.out.println("Account 1 balance: " + acc1.getBalance());
        System.out.println("Account 2 balance: " + acc2.getBalance());
        System.out.println("Total balance: " + (acc1.getBalance() + acc2.getBalance()));
    }
}
