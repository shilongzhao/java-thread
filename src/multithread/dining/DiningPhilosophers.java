package multithread.dining;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class DiningPhilosophers {

    public static void main(String[] args) {
        ReentrantLock[] chopsticks = new Chopstick[5];
        Philosopher[] philosophers = new Philosopher[5];
        ExecutorService executors = Executors.newFixedThreadPool(5);

        for (int i = 0; i < 5; i++) {
            chopsticks[i] = new Chopstick();
        }

        for (int i = 0; i < 5; i++) {
            Philosopher p = new Philosopher(i, chopsticks[i], chopsticks[(i-1 + 5) % 5]);
            executors.submit(p);
        }
        executors.shutdown();

        try{
            executors.awaitTermination(100, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
