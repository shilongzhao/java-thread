package multithread.dining;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Philosopher implements Runnable {
    private int id;
    private Lock l;
    private Lock r;
    public Philosopher(int id, ReentrantLock leftChopstick, ReentrantLock rightChopstick) {
        this.id = id;
        this.l = leftChopstick;
        this.r = rightChopstick;
    }

    @Override
    public void run() {
        if (id % 2 == 0) {
            l.lock();
            r.lock();
            try {
                System.out.println("Philosopher " + id + " eating ..");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                l.unlock();
                r.unlock();
            }
        }
        else {
            r.lock();
            l.lock();

            try {
                System.out.println("Philosopher " + id + " eating ..");
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                r.unlock();
                l.unlock();
            }
        }
    }
}
