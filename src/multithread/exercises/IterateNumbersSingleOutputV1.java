package multithread.exercises;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
有四个线程1、2、3、4。线程1的功能就是输出1，线程2的功能就是输出2，以此类推.........现在有四个文件ABCD。初始都为空。现要让四个文件呈如下格式：
A：1 2 3 4 1 2....
B：2 3 4 1 2 3....
C：3 4 1 2 3 4....
D：4 1 2 3 4 1....
请设计程序
 */
public class IterateNumbersSingleOutputV1 {

    public static void main(String[] args) throws InterruptedException {
        Lock lock = new ReentrantLock();

        Condition w1 = lock.newCondition();
        Condition w2 = lock.newCondition();
        Condition w3 = lock.newCondition();
        Condition w4 = lock.newCondition();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(true) {
                    i++;
                    lock.lock();
                    try {
                        // POSSIBLE SIGNAL LOSS HERE
                        w4.await(); // give up lock and wait until 4 is printed
                        System.out.println(i + " 1");
                        w1.signal();
                    }catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        lock.unlock();
                    }
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(true) {
                    i++;
                    lock.lock();
                    try {
                        w1.await(); // give up lock and wait until 1 is printed
                        System.out.println(i + " 2");
                        w2.signal();
                    }catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        lock.unlock();
                    }

                }
            }
        });

        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(true) {
                    i++;
                    lock.lock();
                    try {
                        w2.await(); // give up lock and wait until 2 is printed
                        System.out.println(i + " 3");
                        w3.signal();
                    }catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        lock.unlock();
                    }
                }
            }
        });

        Thread t4 = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(true) {
                    i++;
                    lock.lock();
                    try {
                        w3.await(); // give up lock and wait until 3 is printed
                        System.out.println( i + " 4");
                        w4.signal();
                    }catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        lock.unlock();
                    }


                }
            }
        });

        t4.start();
        t1.start();
        t2.start();
        t3.start();

        Thread.sleep(100);
        lock.lock();
        try {
            w4.signal();
        } finally {
            lock.unlock();
        }
    }
}
