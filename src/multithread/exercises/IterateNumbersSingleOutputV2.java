package multithread.exercises;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class IterateNumbersSingleOutputV2 {
    public static void main(String[] args) {
        Processor p = new Processor();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                p.print1();
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                p.print2();
            }
        });

        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                p.print3();
                ;
            }
        });

        Thread t4 = new Thread(new Runnable() {
            @Override
            public void run() {
                p.print4();
            }
        });

        t2.start();
        t3.start();
        t4.start();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t1.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

class Processor {
    Lock lock = new ReentrantLock();
    Condition cond = lock.newCondition();

    int currentNum = 4;

    public void print1() {
        while (true) {
            lock.lock();
            try {
                while (currentNum != 4) cond.await();
                System.out.println("1");
                currentNum = 1;
                cond.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public void print2() {
        while (true) {
            lock.lock();
            try {
                while (currentNum != 1) cond.await();
                System.out.println("2");
                currentNum = 2;
                cond.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public void print3() {
        while (true) {
            lock.lock();
            try {
                while (currentNum != 2) cond.await();
                System.out.println("3");
                currentNum = 3;
                cond.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public void print4() {
        while (true) {
            lock.lock();
            try {
                while (currentNum != 3) cond.await();
                System.out.println("4");
                currentNum = 4;
                cond.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
}
