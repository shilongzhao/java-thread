package multithread.exercises.polite_customer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * You need a lock shared by all threads
 * 现成程序中的Test类中的代码在不断地产生数据，然后交给TestDo.doSome()方法去处理，
 * 就好像生产者在不断地产生数据，消费者在不断消费数据。
 * 请将程序改造成有10个线程来消费生成者产生的数据，这些消费者都调用TestDo.doSome()方法去进行处理，
 * 故每个消费者都需要一秒才能处理完，程序应保证这些消费者线程依次有序地消费数据，只有上一个消费者消费完后，
 * 下一个消费者才能消费数据，下一个消费者是谁都可以，但要保证这些消费者线程拿到的数据是有顺序的。原始代码如下
 * http://blog.csdn.net/zhangxiaoxiang/article/details/6648673
 */

public class PoliteCustomer {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Lock lock = new ReentrantLock();
        System.out.println("begin:"+(System.currentTimeMillis()/1000));
        for(int i=0;i<10;i++){  //这行不能改动
            String input = i+"";  //这行不能改动
            executorService.submit(new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        lock.lock();
                        String output = TestDo.doSome(input);
                        System.out.println(Thread.currentThread().getName()+ ":" + output);
                    } finally {
                        lock.unlock();
                    }
                }
            }));

        }
        executorService.shutdown();

        try{
            executorService.awaitTermination(100, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class TestDo {
    public static String doSome(String input){

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String output = input + ":"+ (System.currentTimeMillis() / 1000);
        return output;
    }
}
/*
begin:1504874302
pool-1-thread-1:0:1504874303
pool-1-thread-2:1:1504874304
pool-1-thread-3:2:1504874305
pool-1-thread-4:3:1504874306
pool-1-thread-5:4:1504874307
pool-1-thread-6:5:1504874308
pool-1-thread-7:6:1504874309
pool-1-thread-8:7:1504874310
pool-1-thread-9:8:1504874311
pool-1-thread-10:9:1504874312
 */