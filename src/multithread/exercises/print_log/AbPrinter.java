package multithread.exercises.print_log;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Thread A prints only A; Thread B prints only B
 * print in order A B A B ...
 */
public class AbPrinter {

    public static void main(String[] args) {
        Processor processor = new Processor();
        Thread t1 = new Thread(processor::printA);
        Thread t2 = new Thread(processor::printB);
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch(InterruptedException e) {
            System.out.println("joining failed");
        }
    }

    static class Processor {
        private char thread = 'A';
        private ReentrantLock lock = new ReentrantLock();
        private Condition cond = lock.newCondition();
        public void printA() {
            int i = 0;
            while (i < 1000) {
                try {
                    lock.lock();
                    while (thread != 'A') cond.await();
                    System.out.println(Thread.currentThread() + ": A");
                    thread = 'B';
                    cond.signalAll();

                } catch (InterruptedException e) {
                    System.out.println("interrupted thread A");
                }
                finally {
                    lock.unlock();
                }
                i++;
            }
        }

        public void printB() {
            int j = 0;
            while (j < 1000) {
                try {
                    lock.lock();
                    while (thread != 'B') cond.await();
                    System.out.println(Thread.currentThread() + ": B");
                    thread = 'A';
                    cond.signalAll();

                } catch (InterruptedException e) {
                    System.out.println("interrupted thread A");
                }
                finally {
                    lock.unlock();
                }
                j++;
            }
        }
    }
}
