package multithread.future;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.*;

/**
 * @auther s.zhao@opengate.biz
 */
public class FutureApp {
    public static void main(String[] args) {
        ExecutorService executors = Executors.newCachedThreadPool();

        Future<Integer> future = executors.submit(() -> {
            Random random = new Random();
            int duration = random.nextInt(4000);
            if (duration > 4000)
                throw new IOException("IO timeout");

            System.out.println("Starting ...");
            try {
                Thread.sleep(duration);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Finished");

            return duration;
        });

        executors.shutdown();

        try {
            System.out.println("Duration is: " + future.get()); // use future.get() to retrieve the result
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }


}
