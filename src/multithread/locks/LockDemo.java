package multithread.locks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * author: zhaoshilong
 * date: 08/08/2017
 * Now the program would be much faster than SyncDemo2
 * Because different locks are used for stage1 and stage2 code blocks
 * So that stage1 and stage2 from different threads can be executed at the same time
 * on different threads
 */
public class LockDemo {
    private Random rand = new Random();

    private List<Integer> list1 = new ArrayList<>();
    private List<Integer> list2 = new ArrayList<>();

    private Object lock1 = new Object();
    private Object lock2 = new Object();

    public void stage1() {
        synchronized (lock1) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            list1.add(rand.nextInt(100));
        }
    }



    public void stage2() {
        synchronized (lock2) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        list2.add(rand.nextInt(100));
    }

    public void process() {
        for (int i = 0; i < 1000; i++) {
            stage1();
            stage2();
        }
    }

    public static void main(String[] args) {
        System.out.println("starting ...");
        long start = System.currentTimeMillis();
        LockDemo demo = new LockDemo();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                demo.process();
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                demo.process();
            }
        });

        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("Time taken " + (end - start));
        System.out.println("list1: " + demo.list1.size() + " list2: " + demo.list2.size());
    }

}
