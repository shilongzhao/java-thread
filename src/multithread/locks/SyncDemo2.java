package multithread.locks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * author: zhaoshilong
 * date: 08/08/2017
 * Now no exception, but the execution is slowed down, because thread t2
 * can NOT access function stage1() even when thread t1 has finished stage1().
 * That is because function stage1() and stage2() uses the same lock.
 *
 * So we see the problem: stage1() and stage2() are independent, they operate
 * on different data. However, they cannot be executed in parallel with this solution here.
 */
public class SyncDemo2 {
    private Random rand = new Random();

    private List<Integer> list1 = new ArrayList<>();
    private List<Integer> list2 = new ArrayList<>();

    public synchronized void stage1() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        list1.add(rand.nextInt(100));
    }

    public synchronized void stage2() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        list2.add(rand.nextInt(100));
    }

    public void process() {
        for (int i = 0; i < 1000; i++) {
            stage1();
            stage2();
        }
    }

    public static void main(String[] args) {
        System.out.println("starting ...");
        long start = System.currentTimeMillis();
        SyncDemo2 demo = new SyncDemo2();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                demo.process();
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                demo.process();
            }
        });


        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("Time taken " + (end - start));
        System.out.println("list1: " + demo.list1.size() + " list2: " + demo.list2.size());
    }
}
