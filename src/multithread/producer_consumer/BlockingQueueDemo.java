package multithread.producer_consumer;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * author: zhaoshilong
 * date: 11/08/2017
 */
public class BlockingQueueDemo {
    // no need to use synchronized keyword
    private BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(10); // thread safe structure

    private void producer() throws InterruptedException {
        Random rand = new Random();
        while(true) {
            queue.put(rand.nextInt(100)); // put() will wait until there is empty space in the queue
        }
    }


    private void consumer() throws InterruptedException {
        Random rand = new Random();

        while(true) {
            Thread.sleep(100);

            if (rand.nextInt(10) < 5) { // simulate the take process
                Integer val = queue.take(); // take() is blocked until there is something in the queue

                System.out.println("Taken value: " + val + " Queue size; " + queue.size());
            }
        }
    }

    public static void main(String[] args) {
        BlockingQueueDemo demo = new BlockingQueueDemo();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    demo.producer();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    demo.consumer();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
