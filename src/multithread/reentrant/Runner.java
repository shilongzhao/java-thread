package multithread.reentrant;

import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author s.zhao@opengate.biz
 */

public class Runner {
    private int count = 0;
    private Lock lock = new ReentrantLock(); // once a thread locks a lock, the thread can locks the lock again
    private Condition cond = lock.newCondition();

    private void increment() {
        for (int i = 0; i < 10000; i++)
            count++;
    }

    public void firstThread() throws InterruptedException {
        lock.lock(); // run and acquire lock, synchronized block

        System.out.println("Waiting...");
        cond.await(); // thread give up lock and in WAITING, wait(), wait for secondThread's signal
        // consumers should not contend the lock endlessly (and meaninglessly) they should only contend it when there is something to consume (i.e. be signaled by the producer)
        System.out.println("Waking up!");

        try {
            increment();
        } finally {
            lock.unlock(); // unlock
        }
    }

    public void secondThread() throws InterruptedException {
        Thread.sleep(1000); // sleep 1s, let firstThread have lock first, and firstThread then wait (give up lock)
        lock.lock(); // synchronized blocks

        System.out.println("Press new line to continue!");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        System.out.println("Got return key!");

        cond.signal(); // wake up firstThread, notify(), but firstThread cannot start immediately, secondThread still holds lock

        try {
            increment();
        } finally {
            lock.unlock(); // release the lock, so that firstThread can continue ...
        }
    }

    public void finished() {
        System.out.println("count is " + count);
    }
}
