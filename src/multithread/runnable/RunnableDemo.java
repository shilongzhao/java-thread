package multithread.runnable;

/**
 * author: zhaoshilong
 * date: 07/08/2017
 */

public class RunnableDemo {
    public static void main(String[] args) {
        Thread t1 = new Thread(new Runner()); // feel free to use anonymous inner class or lambda for Runnable
        Thread t2 = new Thread(new Runner());
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            System.out.println("thread interrupted");
        }
    }
}

// to create a thread, you can extends Thread, or implements Runnable and pass it to a thread
class Runner implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println( "Hello " + i);

            try {
                Thread.sleep(15);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
