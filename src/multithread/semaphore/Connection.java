package multithread.semaphore;

import java.util.concurrent.Semaphore;

/**
 * @auther s.zhao@opengate.biz
 */
public class Connection {
    private static Connection connection = new Connection();
    private Semaphore semaphore = new Semaphore(10);
    private int connections = 0;

    private Connection() {

    }

    public static Connection getInstance() {
        return connection;
    }

    public void connect() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            doConnect();
        } finally {
            semaphore.release();
        }
    }

    public void doConnect() {

        synchronized (this) {
            connections++;
            System.out.println("Current connections: " + connections); // current connections will not exceed 10, which id
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (this) {
            connections--;
        }
    }
}
