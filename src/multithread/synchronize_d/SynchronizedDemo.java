package multithread.synchronize_d;

/**
 * author: zhaoshilong
 * date: 07/08/2017
 * The program expect to print out 20000, but the result is smaller.
 * the reason is that the self increment (++) operation is not atomic.
 * Note: volatile does not help here
 *
 */
public class SynchronizedDemo {
    private int count = 0;
    public static void main(String[] args) {
        SynchronizedDemo demo = new SynchronizedDemo();
        demo.doWork();
    }

    public void doWork() {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    count++;
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    count++;
                }
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        /**
         * must wait t1 and t2 to finish and then print the result.
         * that's why we use join()
         */
        System.out.println("count is " + count);
    }
}
