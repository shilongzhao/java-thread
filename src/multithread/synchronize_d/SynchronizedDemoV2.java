package multithread.synchronize_d;

/**
 * author: zhaoshilong
 * date: 08/08/2017
 *
 * now the problem is solved
 * Every java object has an intrinsic lock
 * if u call a synchronized method of an object
 * You have to acquire the intrinsic lock before you
 * can call the method. Only one thread can have the lock
 * at one time and other thread have to wait.
 */

public class SynchronizedDemoV2 {
    private int count = 0;

    private synchronized void increment() {
        count++;
    }

    public static void main(String[] args) {
        SynchronizedDemoV2 demo2 = new SynchronizedDemoV2();
        demo2.doWork();
    }


    public void doWork() {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    increment();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    increment();
                }
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        /**
         * must wait t1 and t2 to finish and then print the result.
         * that's why we use join()
         */
        System.out.println("count is " + count);
    }
}