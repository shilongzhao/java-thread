package multithread.thread;

/**
 * author: zhaoshilong
 * date: 07/08/2017
 */
public class ThreadDemo {
    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.start();
        Runner another = new Runner();
        another.start();
        try {
            runner.join();
            another.join();
        } catch (InterruptedException e) {
            System.out.println("thread interrupted");
        }
        System.out.println("end of main, threads joined");
    }
}
// do not call run() directly. If call run(), it will run in the main thread;
class Runner extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(this.getName() + " " + i);

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}