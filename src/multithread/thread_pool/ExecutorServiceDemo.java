package multithread.thread_pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * author: zhaoshilong
 * date: 09/08/2017
 */
public class ExecutorServiceDemo {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 10; i++) {
            executor.submit(new Processor(i));
        }
        executor.shutdown(); // will not shutdown immediately;

        System.out.println("All tasks submitted!");

        try {
            executor.awaitTermination(10, TimeUnit.SECONDS);
            // In this way the main will not exit before children threads. It must wait
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

class Processor implements Runnable {

    private int id;
    public Processor(int id) {
        this.id = id;
    }
    @Override
    public void run() {
        System.out.println("thread starts " + id);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("thread stops " + id);
    }

}