package multithread.volatiled;

import java.util.Scanner;

/**
 * A problematic demo without using volatile keyword.
 *
 * the problem is that the Processor thread may never stop
 * because the although the main thread modified the running state
 * by calling shutdown, it may have no effect on the Processor thread
 * variable, if the processor thread has cached the running state. And
 * as a result the program will never stop until Processor thread
 * updates its cached variable.
 *
 * The solution is to add volatile keyword to variable running in Processor
 * volatile prevent caching
 */
public class VolatileDemo {
    public static void main(String[] args) {
        Processor proc = new Processor();
        proc.start();

        System.out.println("Press enter to stop: ");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        proc.shutdown();
    }
}

class Processor extends Thread {
    private boolean running = true; // add volatile
    @Override
    public void run() {
        while (running) {
            System.out.println("Running ... ");

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        running = false;
    }
}