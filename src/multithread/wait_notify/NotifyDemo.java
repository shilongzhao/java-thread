package multithread.wait_notify;

import java.util.Scanner;

/**
 * author: zhaoshilong
 * date: 13/08/2017
 */
public class NotifyDemo {

    public void produce() throws InterruptedException {
        synchronized (this) {
            System.out.println("Producer thread running ... ");
            wait(); // give up lock and enters WAITING state,
            // after notify() is called, produce reacquires the lock
            System.out.println("Producer resumed");
        }
    }

    public void consume() throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        Thread.sleep(2000); // the the producer starts first
        synchronized (this) {
            System.out.println("Waiting for return key: ");
            scanner.nextLine();
            System.out.println("Return key pressed");
            notify(); // notify() can only be called inside the synchronized block
            // notifies one WAITING thread that lock on the same lock object.
            // after notify(), the lock is NOT relinquished immediately
            // after notify(), the produce() thread is in BLOCKED state, because this consumer thread does not relinquish lock yet
        }
        // now the producer acquires the lock, moving from BLOCKED to RUNNABLE

    }

    public static void main(String[] args) {
        NotifyDemo demo = new NotifyDemo();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    demo.produce();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    demo.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
