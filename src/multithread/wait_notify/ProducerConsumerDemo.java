package multithread.wait_notify;

import java.util.LinkedList;
import java.util.Random;

/**
 * author: zhaoshilong
 * date: 13/08/2017
 */
public class ProducerConsumerDemo {

    public static void main(String[] args) {
        final Processor processor = new Processor();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor.produce();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


class Processor {
    private LinkedList<Integer> list = new LinkedList<>();
    private final int LIMIT = 10;

    private Object lock = new Object();

    public void produce() throws InterruptedException{
        int value = 0;
        while(true) {

            synchronized (lock) {
                while (list.size() == LIMIT) {
                    lock.wait();
                }
                list.add(value++);
                lock.notify(); // when adds a new value, let consumer know
            }
        }
    }

    public void consume() throws InterruptedException {
        Random rand = new Random();
        while(true) {
            synchronized (lock) {

                while (list.size() == 0) {
                    lock.wait();
                }

                System.out.print("List size is " + list.size());
                int value = list.remove();
                System.out.println("; Value is " + value);
                lock.notify(); // when consume, let producer konw
            }

            Thread.sleep(rand.nextInt(1000));
        }
    }
}